/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mtbit.cms.web.rest.vm;
